/*
 * TestNG class for Passenger 
 */
package dev.sol.apps.model;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Solomon
 */
public class PassengerNGTest {
    
    public PassengerNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getCurrentFloor method, of class Passenger.
     */
    @Test
    public void testGetCurrentFloor() {
        System.out.println("getCurrentFloor");
        Passenger instance = new Passenger("A", 1, 2);
        int expResult = 1;
        int result = instance.getCurrentFloor();
        assertEquals(result, expResult);        
    }

    /**
     * Test of setCurrentFloor method, of class Passenger.
     */
    @Test
    public void testSetCurrentFloor() {
        System.out.println("setCurrentFloor");
        int currFloor = 0;
        Passenger instance = new Passenger();
        instance.setCurrentFloor(currFloor);       
    }

    /**
     * Test of getDestinationFloor method, of class Passenger.
     */
    @Test
    public void testGetDestinationFloor() {
        System.out.println("getDestinationFloor");
        Passenger instance = new Passenger("A", 1, 2);
        int expResult = 1;
        int result = instance.getDestinationFloor();
        assertEquals(result, expResult);        
    }

    /**
     * Test of setDestinationFloor method, of class Passenger.
     */
    @Test
    public void testSetDestinationFloor() {
        System.out.println("setDestinationFloor");
        int destFloor = 0;
        Passenger instance = new Passenger();
        instance.setDestinationFloor(destFloor);        
    }

    /**
     * Test of getSourceFloor method, of class Passenger.
     */
    @Test
    public void testGetSourceFloor() {
        System.out.println("getSourceFloor");
        Passenger instance = new Passenger("A", 1, 2);
        int expResult = 2;
        int result = instance.getSourceFloor();
        assertEquals(result, expResult);        
    }

    /**
     * Test of setSourceFloor method, of class Passenger.
     */
    @Test
    public void testSetSourceFloor() {
        System.out.println("setSourceFloor");
        int srcFloor = 0;
        Passenger instance = new Passenger();
        instance.setSourceFloor(srcFloor);        
    }

    /**
     * Test of getName method, of class Passenger.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Passenger instance = new Passenger("Name", 1);
        String expResult = "Name";
        String result = instance.getName();
        assertEquals(result, expResult);       
    }

    /**
     * Test of toString method, of class Passenger.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Passenger instance = new Passenger("A", 1);
        String expResult = "A";
        String result = instance.toString();
        assertEquals(result, expResult);        
    }

    /**
     * Test of compareTo method, of class Passenger.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Passenger pass = new Passenger("A", 5);
        Passenger instance = new Passenger("B", 5);
        int expResult = 0;
        int result = instance.compareTo(pass);
        assertNotEquals(result, expResult);       
    }
    
    /**
     * Test of compareTo method, of class Passenger.
     */
    @Test
    public void testCompareToEquals() {
        System.out.println("compareTo");
        Passenger pass = new Passenger("A", 5);
        Passenger instance = new Passenger("A", 5);
        int expResult = 0;
        int result = instance.compareTo(pass);
        assertEquals(result, expResult);       
    }
    
    /**
     * Test of compareTo method, of class Passenger.
     */
    @Test
    public void testCompareToBefore() {
        System.out.println("compareTo");
        Passenger pass = new Passenger("A", 6);
        Passenger instance = new Passenger("A", 5);
        int expResult = -1;
        int result = instance.compareTo(pass);
        assertEquals(result, expResult);       
    }
    
    /**
     * Test of compareTo method, of class Passenger.
     */
    @Test
    public void testCompareToAfter() {
        System.out.println("compareTo");
        Passenger pass = new Passenger("A", 5);
        Passenger instance = new Passenger("A", 6);
        int expResult = 1;
        int result = instance.compareTo(pass);
        assertEquals(result, expResult);       
    }
    
}
