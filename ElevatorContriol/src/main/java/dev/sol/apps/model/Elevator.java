/*
 * Class representing Elevator
 */
package dev.sol.apps.model;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Solomon
 */
public class Elevator implements Runnable {

    /**
     * Current elevator state.
     */
    protected ElevatorStatesEnum state;

    /**
     * Default travel direction.
     */
    protected ElevatorStatesEnum direction;

    /**
     * Current floor (1-based).
     */
    protected int currentFloor = 1;

    // floor count increment
    protected AtomicInteger count = new AtomicInteger();

    /**
     * Building Floor count
     */
    private int totalFloors = 1;

    private Building elevatorBld;

    private final static Logger logger = Logger.getLogger(Elevator.class.getName());

    /**
     * Instantiates a new Elevator instance.
     *
     * @param bldg
     * @param capacity - currently not accounted
     */
    public Elevator(Building bldg, int capacity) {

        this.currentFloor = 1;
        this.direction = ElevatorStatesEnum.UP;
        this.elevatorBld = bldg;

        this.totalFloors = elevatorBld.getFloorCount();
    }

    /**
     * @return direction Current direction of move
     */
    public ElevatorStatesEnum getDirection() {
        return direction;
    }

    /**
     * Sets move direction of the elevator
     *
     * @param direction The new move direction
     */
    public void setDirection(ElevatorStatesEnum direction) {

        if (direction != this.direction) {
            System.out.printf("Changing elevator direction to: %s\n", direction);
        }
        this.direction = direction;
    }

    /**
     * Change the direction of the elevator.
     *
     */
    protected void changeDirection() {

        if (currentFloor == elevatorBld.getFloorCount()) {
            setDirection(ElevatorStatesEnum.DOWN);
            setCurrentState(ElevatorStatesEnum.DOWN);
        } else if (currentFloor == 1) {
            setDirection(ElevatorStatesEnum.UP);
            setCurrentState(ElevatorStatesEnum.UP);
        }

    }

    public int getCount() {
        return this.count.get();
    }

    /**
     * Returns the current floor of the elevator
     *
     * @return Current floor
     *
     */
    public int getCurrentFloor() {
        return this.currentFloor;
    }

    /**
     * Sets the current floor of the elevator
     *
     * @param floor Current floor
     *
     */
    public void setCurrentFloor(int floor) {
        if (floor <= elevatorBld.getFloorCount()) {
            this.currentFloor = floor;
        }
    }

    /**
     * Returns the current state of the elevator
     *
     * @return Current state
     *
     */
    public ElevatorStatesEnum getCurrentState() {
        return this.state;
    }

    /**
     * Sets the current state of the elevator
     *
     * @param state Current state
     *
     */
    public void setCurrentState(ElevatorStatesEnum state) {
        this.state = state;
    }

    /**
     * Start moving the elevator to next floor.
     */
    protected void move() {

        if (direction == ElevatorStatesEnum.UP) {            
            count.incrementAndGet();
            setCurrentState(ElevatorStatesEnum.UP);
        }

        if (direction == ElevatorStatesEnum.DOWN) {
            count.decrementAndGet();            
            setCurrentState(ElevatorStatesEnum.DOWN);
        }

        this.currentFloor = getCount();

        setCurrentFloor(this.currentFloor);

        logger.log(Level.INFO, "Elevator has moved to floor {0}",
                new Object[]{getCurrentFloor()});

        /**
         * change the direction of elevator move on reaching either the top
         * floor or the base
         */
        if (this.currentFloor <= 1
                || this.currentFloor >= elevatorBld.getFloorCount()) {
            changeDirection();
        }

    }

    /**
     * Stop the elevator.
     */
    protected void stop() {
        /* stop the elevator only if there is a valid request to load or 
         * unload passengers on the current floor; else keep moving
         */
        if (isStopRequestValid()) {
            logger.log(Level.INFO, "Elevator has stopped at floor {0}",
                    new Object[]{getCurrentFloor()});

        } else {
            logger.log(Level.INFO, "Elevator has not stopped at floor {0}",
                    new Object[]{getCurrentFloor()});

            // move on to the next floor
            //move();
        }

    }

    /**
     * Checks if the elevator should stop on current floor.
     *
     * TODO - consider stopping based on valid passenger requests and the
     * direction,state of the elevator move
     *
     * @return true always as of now
     *
     */
    private boolean isStopRequestValid() {
        return true;
    }

    /**
     * Method to handle the case where the doors open on a particular floor.
     *
     * <p>
     * Unload passengers traveling to this floor, and load passengers waiting on
     * this floor for the elevator.
     */
    protected void openDoor() {

        // first, set the state to OPEN
        setCurrentState(ElevatorStatesEnum.OPEN);

        logger.log(Level.INFO, "Elevator doors open on floor {0}",
                new Object[]{getCurrentFloor()});

        if (elevatorBld != null) {
            elevatorBld.unloadPassengers(this);
        }

    }

    /**
     * Method to handle the case where the doors close on a particular floor.
     *
     */
    protected void closeDoor() {

        // first, set the state to CLOSE
        setCurrentState(ElevatorStatesEnum.CLOSE);

        logger.log(Level.INFO, "Elevator doors close on floor {0}",
                new Object[]{getCurrentFloor()});

        try {
            // allow time to settle
            Thread.sleep(7000);
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }

        // start moving again
        move();

    }

    /**
     * Move the elevator until all floors are swept atleast once in both
     * directions
     */
    public void run() {

        logger.log(Level.INFO, "STARTING Elevator Run");

        for (int i = 0; i < totalFloors * 2; i++) {
            move();
            stop();
            openDoor();
            closeDoor();
        }
        
        elevatorBld.printSummary();

        logger.log(Level.INFO, "STOPPING Elevator Run");

    }

}
