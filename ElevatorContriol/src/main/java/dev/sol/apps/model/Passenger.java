/*
 * Class representing elevator Passenger
 */
package dev.sol.apps.model;

/**
 *
 * @author Solomon
 */
public class Passenger implements Comparable<Passenger> {

    /**
     * The passenger identity. Not guaranteed to be unique and is used for
     * human-readable representation only.
     *
     *
     */
    private String name;

    /**
     * The floor passenger is currently on (1-based)
     *
     */
    private int currentFloor =1;

    /**
     * The source floor
     *
     */
    private int sourceFloor;

    /**
     * The destination floor
     *
     */
    private int destinationFloor;

    /**
     * Default constructor, not to be called from any outside code
     *
     */
    public Passenger() {

    }

    /**
     * This constructor would create a Passenger planning to get a ride to the
     * given floor from the source floor
     *
     * @param name The passenger
     * @param destFloor The destination floor
     *
     */
    public Passenger(String name, int destFloor) {
        this.name = name;
        this.sourceFloor = 1;
        this.destinationFloor = destFloor;
    }

    /**
     * This constructor would create a Passenger planning to get a ride to the
     * given floor requesting from the source floor while the elevator is in
     * transit
     *
     * @param name The passenger
     * @param srcFloor The source floor
     * @param destFloor The destination floor
     *
     */
    public Passenger(String name, int destFloor, int srcFloor) {
        this.name = name;        
        this.destinationFloor = destFloor;
        this.sourceFloor = srcFloor;
    }

    /**
     * Returns the current floor of the passenger
     *
     * @return Current floor
     *
     */
    protected int getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Sets the current floor (1-based) of the passenger
     *
     * @param currFloor Current floor
     *
     */
    protected void setCurrentFloor(int currFloor) {
        if (currFloor >= 1) {
            this.currentFloor = currFloor;
        }
    }

    /**
     * Returns the destination floor of the passenger
     *
     * @return Destination floor
     *
     */
    protected int getDestinationFloor() {
        return destinationFloor;
    }

    /**
     * Sets the destination floor (1-based) of the passenger
     *
     * @param destFloor Destination floor
     *
     */
    protected void setDestinationFloor(int destFloor) {
        if (destFloor >= 1) {
            this.destinationFloor = destFloor;
        }
    }

    /**
     * Returns the source floor from where the passenger requested to take the
     * elevator
     *
     * @return Source floor requested from
     *
     */
    protected int getSourceFloor() {
        return sourceFloor;
    }

    /**
     * Sets the current floor (1-based) of the passenger
     *
     * @param srcFloor Source floor requested from
     *
     */
    protected void setSourceFloor(int srcFloor) {
        if (srcFloor >= 1) {
            this.sourceFloor = srcFloor;
        }
    }

    /**
     * Returns the name of the passenger
     *
     * @return Passenger name
     *
     */
    protected String getName() {
        return name;
    }

    /**
     * Human readable representation of passenger
     *
     *
     * @return String representation
     *
     */
    @Override
    public String toString() {
        String displayName;
        if (name == null || name.isEmpty()) {
            displayName = String.format("Passenger %s", name);
        } else {
            displayName = name;
        }

        return displayName;
    }

    @Override
    public int compareTo(Passenger pass) {
        
        final int BEFORE = -1;
        final int AFTER = 1;
        final int EQUAL = 0;

        if (this.destinationFloor == pass.getDestinationFloor()) {
            return this.name.compareTo(pass.getName());
        }
        
        if (this.destinationFloor < pass.getDestinationFloor()) {
            return BEFORE;
        } 
        
        if (this.destinationFloor > pass.getDestinationFloor()) {
            return AFTER;
        }

        return EQUAL;
    }

}
