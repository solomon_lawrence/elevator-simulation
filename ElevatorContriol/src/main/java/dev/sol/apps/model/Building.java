/*
 * Class representing Elevator Building
 */
package dev.sol.apps.model;

import dev.sol.apps.exceptions.ElevatorControlException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Solomon
 */
public class Building {

    private Elevator[] elevators;

    private BlockingQueue upQ = new PriorityBlockingQueue<Passenger>();
    private BlockingQueue downQ = new PriorityBlockingQueue<Passenger>();
    private BlockingQueue waitQ = new PriorityBlockingQueue<Passenger>();

    private final static Logger logger = Logger.getLogger(Building.class.getName());

    /**
     * Maximum elevator capacity (persons).
     */
    protected static final int CAPACITY = 5;

    /**
     * Maximum floors (persons).
     */
    private int max_floors;

    /**
     * Initializes an elevator building with the given number of elevators and
     * floors.
     *
     * @param lifts the number of elevators
     * @param floors the number of floors
     */
    public Building(int elves, int floors) {

        // initialize all the elevators
        this.elevators = new Elevator[elves];

        for (int i = 0; i < elves; i++) {
            this.elevators[i] = new Elevator(this, CAPACITY);
        }

        // floor count
        this.max_floors = floors;       
    }

    /**
     * @return max_floors Maximum Count of floors in the building
     */
    public int getFloorCount() {
        return max_floors;
    }

    /**
     * Determines whether a given floor is valid for this building.
     *
     * @param floor the floor index
     * @return true if the floor is valid, false otherwise
     *
     */
    private boolean isValidFloor(int floor) {
        return floor >= 1 && floor < this.getFloorCount();
    }

    /**
     * Adds a passenger to the building queue on their start floor. This method
     * checks to make sure the passenger has valid start and destination floors
     * for this elevator building.
     *
     * @param user the passenger to add
     */
    public void addPassenger(Passenger user) {

        logger.log(Level.INFO, "Boarding {0} to floor {1}",
                new Object[]{user, user.getDestinationFloor()});

        if (this.isValidFloor(user.getSourceFloor())
                && this.isValidFloor(user.getDestinationFloor())
                && user.getSourceFloor() != user.getDestinationFloor()) {

            if (user.getSourceFloor() < user.getDestinationFloor()) {
                this.upQ.offer(user);
            } else {
                this.downQ.offer(user);
            }
        } else {
            logger.log(Level.SEVERE,
                    "Failed to add passenger to the building Qs");

        }
    }

    /**
     * Creates an on-floor request for a given Elevator by a given passenger.
     *
     * @param elevator The elevator being called
     * @param passenger The passenger requesting service
     */
    public void pressRequest(Elevator elevator, Passenger passenger) {

        int destFloor = passenger.getDestinationFloor();
        int floorNumber = elevator.getCurrentFloor();

        logger.log(Level.INFO, "{0} requests service at floor {1} to "
                + "destination floor {2}",
                new Object[]{passenger, floorNumber, destFloor});

        if (this.isValidFloor(passenger.getSourceFloor())
                && this.isValidFloor(passenger.getDestinationFloor())
                && passenger.getSourceFloor() != passenger.getDestinationFloor()) {

            if (destFloor > floorNumber) {
                this.upQ.offer(passenger);
            } else if (destFloor < floorNumber) {
                this.downQ.offer(passenger);
            } else {
                this.waitQ.offer(passenger);
            }

            passenger.setCurrentFloor(floorNumber);

        } else {
            logger.log(Level.SEVERE,
                    "Failed to add passenger to the building Qs");

        }

    }

    /**
     * Unload passengers traveling to this floor, and load passengers waiting on
     * this floor for the elevator.
     *
     * @param elevator The elevator
     */
    protected void unloadPassengers(Elevator elevator) {

        int currFloor = elevator.getCurrentFloor();
        //BlockingQueue<Passenger> que = new PriorityBlockingQueue<Passenger>();
        ElevatorStatesEnum direction = elevator.getDirection();

        logger.log(Level.INFO, "Unload passengers from floor {0} when the "
                + "elevator is moving {1}",
                new Object[]{currFloor, direction});

        BlockingQueue<Passenger> que
                = direction == ElevatorStatesEnum.UP ? upQ : downQ;
//        
//        if (direction == ElevatorStatesEnum.UP) {
//            que = upQ;
//        } else if (direction == ElevatorStatesEnum.DOWN) {
//            que = downQ;
//        }

        try {

            /**
             * Check to make sure the elevator's current floor equals the
             * passenger destination floor before unloading the passenger.
             */
            /**
             * **********************************************************
             *
             * TODO - review logic here; running into control flow issues when
             * iterating Q with a single element
             *
             * Passenger nextUser = (Passenger) que.peek();
             *
             * logger.log(Level.INFO, "Checking for inQ passenger {0} exit
             * criteria " + "on floor {1}", new Object[]{nextUser, currFloor});
             *
             * while (nextUser != null && !que.isEmpty()) {
             *
             * logger.log(Level.INFO, "Passenger {0} target destination " +
             * "floor is {1}", new Object[]{nextUser,
             * nextUser.getDestinationFloor()});
             *
             * if (nextUser.getDestinationFloor() == currFloor) {
             * logger.log(Level.INFO, "Passenger {0} exiting elevator from " +
             * "floor {1}", new Object[]{nextUser, currFloor});
             * que.remove(nextUser); }
             *
             * nextUser = (Passenger) que.peek(); * }
             * ********************************************************
             */
            Passenger[] passArr = que.toArray(new Passenger[que.size()]);

            for (Passenger nextUser : passArr) {

                logger.log(Level.INFO, "Checking for inQ passenger {0} exit "
                        + "criteria on floor {1}",
                        new Object[]{nextUser, currFloor});

                if (nextUser.getDestinationFloor() == currFloor) {
                    logger.log(Level.INFO, "Passenger {0} exiting elevator from "
                            + "floor {1}", new Object[]{nextUser, currFloor});
                    que.remove(nextUser);
                }
            }

            /**
             * Next, check to make sure the waiting passengers can get on the
             * elevator.
             */
            Passenger[] waitArr = (Passenger[]) waitQ.toArray(new Passenger[waitQ.size()]);

            for (Passenger waitUser : waitArr) {

                logger.log(Level.INFO, "Checking for waitQ passenger {0} entry "
                        + "criteria on floor {1}",
                        new Object[]{waitUser, currFloor});

                if (waitUser.getSourceFloor() == currFloor) {
                    logger.log(Level.INFO, "Passenger {0} entering elevator at "
                            + "floor {0}", new Object[]{waitUser, currFloor});

                    //add passenger to the upQ/downQ
                    this.addPassenger(waitUser);

                    // remove passenger from the waitingQ on floor
                    waitQ.remove(waitUser);
                }
            }

        } catch (Throwable th) {
            logger.log(Level.SEVERE,
                    th.getMessage());
        }

        return;

    }

    public void printSummary() {

        System.out.println ("================================================");
        System.out.println ("=========== SIMULATOR RUN SUMMARY ==============");

        System.out.println ("Number of Elevators: 1");        
        System.out.println ("Number of Floors: "+getFloorCount());
        
        System.out.println ("Number of Passengers moving UP: "+upQ.size());
        System.out.println ("Passengers moving UP: "+upQ);
        System.out.println ();
        
        System.out.println ("Number of Passengers moving DOWN: "+downQ.size());
        System.out.println ("Passengers moving DOWN: "+downQ);
        System.out.println ();
        
        System.out.println ("Number of Passengers waiting on floor: "+waitQ.size());
        System.out.println ("Passengers waiting on floor: "+waitQ);
        System.out.println ();
        
        System.out.println ("================================================");
    }

}
