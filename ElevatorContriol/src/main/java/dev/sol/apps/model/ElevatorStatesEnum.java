/*
 * Enum representing the state of Elevator
 */
package dev.sol.apps.model;

/**
 *
 * @author Solomon
 */
public enum ElevatorStatesEnum {

    IDLE, UP, DOWN, OPEN, CLOSE;

    /**
     * @see java.lang.Enum#toString()
     * @return Human-readable representation of the Elevator STATES
     */
    @Override
    public String toString() {

        switch (this) {
            case IDLE:
                return "IDLE";

            case UP:
                return "UP";

            case DOWN:
                return "DOWN";

            case OPEN:
                return "OPEN";

            case CLOSE:
                return "CLOSE";

            default:
                throw new RuntimeException("STATES: value not supported");
        }

    }

}
