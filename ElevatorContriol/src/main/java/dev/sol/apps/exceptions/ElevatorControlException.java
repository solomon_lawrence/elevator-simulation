/*
 * Exception indicating the elevator control cannot handle the requested service
 */
package dev.sol.apps.exceptions;

/**
 *
 * @author Solomon
 */
public class ElevatorControlException extends Exception {
    
    /**
     * Human-readable message
     * 
     * @return 
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage()
    {
        return "Elevator cannot process request";
    }

    
}
