/*
 * Class representing the basic elevator control system simulation.
 */
package dev.sol.apps;

import dev.sol.apps.exceptions.ElevatorControlException;
import dev.sol.apps.model.Building;
import dev.sol.apps.model.Elevator;
import dev.sol.apps.model.Passenger;

/**
 *
 * @author Solomon
 */
public class ElevatorControlMain {

    private int elves;

    private Building elvBld;
    private Elevator elevator;

    // Default constants for basic run
    public static final int NUM_ELEVATORS = 1;
    public static final int NUM_FLOORS = 5;
    public static final int NUM_CAPACITY = 5;

    /**
     * Initializes an elevator simulation.
     *
     * @param elves the number of elevators in the simulation
     * @param floors the number of floors in the simulation
     * @param capacity the passenger capacity of each elevator
     */
    public ElevatorControlMain(int elves, int floors, int capacity) {

        this.elvBld = new Building(elves, floors);

        this.elevator = new Elevator(elvBld, capacity);

    }

    /**
     * Test 1: Test how the simulation handles the case when the queued
     * passengers are expected to board/unload on most floors
     *
     */
    private void test1() throws ElevatorControlException {
        elvBld.addPassenger(new Passenger("BAC", 5, 1));
        elvBld.addPassenger(new Passenger("JPM", 4, 2));
        elvBld.addPassenger(new Passenger("WFA", 2, 3));

        // reject this case; invalid destination floor
        elvBld.addPassenger(new Passenger("C", 7, 3));
        
        elvBld.addPassenger(new Passenger("AEO", 3, 3));

        new Thread(elevator).start();
        
    }

    /**
     * Test 1: Test how the simulation handles the case when the queued
     * passengers make requests to stop on a floor
     *
     */
    private void test2() throws ElevatorControlException {

        elvBld.addPassenger(new Passenger("BAC", 5));
        
        elvBld.pressRequest(elevator, new Passenger("WMT", 3));
        elvBld.pressRequest(elevator, new Passenger("MSFT", 4, 2));

        // reject this case; invalid destination floor
        elvBld.pressRequest(elevator, new Passenger("AXP", 7));        

        new Thread(elevator).start();
    }

    /**
     * The main method.
     *
     * @param args Currently ignored
     */
    public static void main(String[] args) {

        ElevatorControlMain simulation
                = new ElevatorControlMain(NUM_ELEVATORS, NUM_FLOORS,
                        NUM_CAPACITY);

        int simNum = 1;

        if (args.length > 0) {
            try {
                simNum = Integer.parseInt(args[0]);
            } catch (NumberFormatException ex) {
                System.out.println("Bad input argument, running simulation case#1");
            }
        }

        try {
            switch (simNum) {

                case 1:
                    simulation.test1();
                    break;
                case 2:
                    simulation.test2();
                    break;
                default:
                    simulation.test1();
                    break;
            }
        } catch (ElevatorControlException e) {
            System.out.println(e.getMessage());
        }

    }
}
