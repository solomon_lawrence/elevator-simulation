# elevator-simulation
Java implementation to simulate basic elevator control.

The scope of the implementation is for a N-floor (N>1) building with 1 elevator, and can accept request from passengers. The elevator can change states to IDLE/UP/DOWN/OPEN/CLOSE based on decisions made by the control system. 

Sample Program
--------------
The implementation depends/uses the following technology stack:
- Java7
- Apache Maven
- TestNG

#### Basic Elevator Control System

The basic elevator control system implementation is modeled around the entity Building, the actors Elevator and Passengers.The high level design pivots on using 3 shared PriorityQueues, to track and place the itenary (source floor,destination) details of the passenger. This implementation uses PriorityQueues as the data structure as it provides constant time for retrieval methods and Olog(n) performance for common enqueing and dequeing operations. The goal must be to minimize the average passenger wait time. However, this implementation does not factor in the wait-time and capacity variables. 

Under the program's top-level package, you will see the main Class [ElevatorControlMain.java] (https://github.com/sonelson/elevator-simulation/blob/master/ElevatorContriol/src/main/java/dev/sol/apps/ElevatorControlMain.java). The main method initializes and executes the test scenarios simulating the elevator control system.

An [Elevator](https://github.com/sonelson/elevator-simulation/blob/master/ElevatorContriol/src/main/java/dev/sol/apps/model/ElevatorControlMain.java) Class is created with the Building it is operating in, and a maximum person capacity. The run method implements the basic scheduling for the elevator. It first services all the requests (for e.g. boarding/unloading passengers) going up and then switches direction to service requests the other way.

The [Passenger] (https://github.com/sonelson/elevator-simulation/blob/master/ElevatorContriol/src/main/java/dev/sol/apps/model/Passenger.java) represents the actor sending key data events to the control system.

The [Building] (https://github.com/sonelson/elevator-simulation/blob/master/ElevatorContriol/src/main/java/dev/sol/apps/model/Building.java) class describes the simulation environment that Passenger and Elevator interacts with. 

##### Extensions

To extending this basic system for supporting multiple elevators, would consider modifying the current interaction model as a producer-consumer. This means introducing a more centralized data control/access mehanism to determine efficient elevator movements and reduce passenger wait times. 


#### Running the Simulation

Change to your local project work directory. Clone the git repository.

    $ git clone https://github.com/sonelson/elevator-simulation.git

To build the application into a JAR, invoke the following maven command in your development console:

    $ mvn clean install

A successful build output will produce the packaged JAR (located under <project-work-dir>/elevator-simulation/ElevatorContriol/target/ElevatorControl-1.0-SNAPSHOT.jar)

To run the simulation program

    $ mvn exec:java -Dexec.mainClass="dev.sol.apps.ElevatorControlMain" -Dexec.args=1

To run test-case#2, specify '-Dexec.args=2' in the mvn command line.

#### Testing
The current version has 2 basic test cases to simulate the elevator control system. The default run will execute test-case#1. The simulation program can be further enhanced to read input test parameters from an external file. 

Here is a snippet from the log trace of the simulator run, printing the summary.

================================================
=========== SIMULATOR RUN SUMMARY ==============
Number of Elevators: 1
Number of Floors: 5
Number of Passengers moving UP: 1
Passengers moving UP: [MSFT]

Number of Passengers moving DOWN: 0
Passengers moving DOWN: []

Number of Passengers waiting on floor: 0
Passengers waiting on floor: []

================================================


